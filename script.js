
// 3. Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
// - If the total of the two numbers is less than 10, add the numbers
// - If the total of the two numbers is 10 - 19, subtract the numbers
// - If the total of the two numbers is 20 - 29 multiply the numbers
// - If the total of the two numbers is greater than or equal to 30, divide the numbers


let assignNumA = parseInt(prompt(`Provide a number`));
let assignNumB = parseInt(prompt(`Provide another number`));

let numAnumB = assignNumA + assignNumB;
let finalNumber
let finalText

if (numAnumB <= 10 ) {
	finalNumber = assignNumA + assignNumB
	finalText = `total`
} else if (numAnumB < 20 ) {
	finalNumber = assignNumA - assignNumB
	finalText = `difference`
} else if (numAnumB < 30) {
	finalNumber = assignNumA * assignNumB
	finalText = `product`
} else if (numAnumB >= 30) {
	finalNumber = assignNumA / assignNumB
	finalText = `quotient`
}

console.log(finalNumber)

// 4. Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.

// Interpolation - paglagay ng variable sa string
alert(`The ${finalText} of the two numbers are ${finalNumber}`)


// 5. Prompt the user for their name and age and print out different alert messages based on the user input:
// -  If the name OR age is blank/null, print the message are you a time traveler?
// -  If the name AND age is not blank, print the message with the user’s name and age.

let name = prompt(`What is your name?`);
let age = prompt(`What is your age?`);

if( name === "" || name === null || age > 100 || age == "") {
	alert("Are you a time traveler?")
} else if (name !== "" && age !== "" ){
	alert("Hello "+ name + ". Your age is " + age);
}


function isLegalAge(age) {
	age >= 18 ? alert("You are of legal age.") : alert("You are not allowed here.") 
}

isLegalAge(age)

switch(age) {
	case "18":
		alert("You are now allowed to party.");
		break;

	case "21":
		alert("You are now part of the adult society.");
		break;

	case "65":
		alert("We thank you for your contribution to society");
		break;

	default:
		alert("Are you sure you're not an alien?");
		break;

}


try {
	islegag(age);
} catch(error) {
	console.warn(error.message);
} finally {
	isLegalAge(age)
}

